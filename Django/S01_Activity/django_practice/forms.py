class AddTaskForm(forms.Form):
	username = forms.CharField(label="Username", max_length=50)
	first_name = forms.CharField(label="First Name", max_length=200)
	last_name = forms.CharField(label="Last Name", max_length=200)
	email = forms.CharField(label="Email", max_length=200)
	password = forms.CharField(label="Password", max_length=200)